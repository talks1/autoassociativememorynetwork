import { describe } from 'riteway'

import  { AANetwork } from '../src/aanetwork'

describe('AANetwork', async (assert) => {
    const training = [[1,1,0],[1,1,0],[0,0,1]]
    let aa = AANetwork(3)

    aa.Train(training)
    
    let w = aa.Weights()

    assert({
        given: 'some training',
        should: 'weights are correct',
        actual: `${w}`,
        expected: '2,2,0,2,2,0,0,0,1'
      })

    let y = aa.Compute([1,1,-1])
    
    assert({
        given: 'some weights',
        should: 'compute output vector',
        actual: `${y}`,
        expected: '1,1,-1'
      })

      let y2 = aa.Compute([-1,-1,1])

      assert({
        given: 'some weights',
        should: 'compute output vector',
        actual: `${y2}`,
        expected: '-1,-1,1'
      }) 


    //   let y3 = aa.compute([-1,0,1])
    //   console.log(y3)
})

