let debug = ()=>{}


export const AAForce = (size,scale,AANetwork) => {

    let aanetwork = AANetwork(size)
    
    let delta =(current,target,k)=>{
        return current.map((c,i)=>{
            return ((target[i]-c)/2)*k
        })
    }

    return {
        Train: (training)=>{
            debug(training)
            let t  = training.map((memory)=>memory.map((location)=>scale(location)))
            
            aanetwork.Train(t)
        },
        Target: (thenodes,k)=>{
            let n  = thenodes.map((location)=>scale(location))
            let c = aanetwork.Compute(n)
            let t = c.map((location)=>scale.invert(location))
            let d = delta(thenodes,t,k)
            debug(d)
            return {
                target: t,
                delta: d
            }
        }   
    }
}
