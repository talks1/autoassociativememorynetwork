import React, {useEffect} from 'react';
import { render} from 'react-dom';
import { AANetwork } from '../../src/aanetwork.js'
import { AAForce } from '../../src/force.js'
import { scaleLinear } from 'd3-scale'
import { select, event } from 'd3-selection'
import { drag} from 'd3-drag'
import {forceSimulation, forceLink, forceCenter, forceManyBody} from 'd3-force'
import {} from 'd3-transition'

const App = () => {

    const dist = link => {
        return Math.sqrt(Math.pow(link.source.x -link.target.x,2))
    }
    const scale = scaleLinear().domain([25,75]).range([-1,1])

    const height = 100
    const width = 100

    const data = {
        nodes: [{id: "Node1", x:0},{id: "Node2", x:10},{id: "Node3",x:20},{id: "Node4",x:50}],
        links: [{source: "Node1", target: "Node2", i: 0, value: 0},
                {source: "Node2", target: "Node3", i: 1, value: 0},
                {source: "Node3", target: "Node4", i: 2, value: 0}]
    }

    const links = data.links
    const nodes = data.nodes

    const dragSetup = (simulation,repaint) => {
    
        function dragstarted(d) {
            if (!event.active) {
                if(!simulationMgr.isStopped()){
                    simulation
                        .alphaTarget(0.3)
                        .restart();
                    d.fx = d.x;
                    d.fy = d.y;
                }
            }
        }
        
        function dragged(d) {
            
            if(!simulationMgr.isStopped()){
                d.fx = event.x;
                d.fy = event.y;
            } else {
                d.x = event.x;
                d.y = event.y;
                repaint()
            }
        }
        
        function dragended(d) {
            if (!event.active) {
                if(!simulationMgr.isStopped()){
                    simulation.alphaTarget(0);
                    d.fx = null;
                    d.fy = null;
                }  
            }
        }
    
        return drag()
            .on("start", dragstarted)
            .on("drag", dragged)
            .on("end", dragended);
    }

    const SimulationMgr = ()=>{

        let aa = null
        let memories = []
        let isStopped=0
        let simulation = null

        let repaint = null

        return {
            setRepaint: (r)=>repaint = r,
            setSimulation: (x)=>simulation = x,
            forget: ()=>{
                aa = AAForce(8,scale,AANetwork,0.1)
                memories = []
                showMemories(memories)
            },
            restore: (i)=>{
                let memory = memories[i]
                console.log(memory.data)
                console.log(memories)
                for(var i=0;i<memory.data.length/2;i++){
                    nodes[i].x= memory.data[i*2]
                    nodes[i].y= memory.data[i*2+1]
                }
                repaint()
            },
            capture: ()=>{
                let ns = nodes.map((n)=>{
                    return [n.x,n.y]
                })
                let n = ns.flat()
                
                let memory = {
                    id: memories.length,
                    data: n
                }
                console.log(memory)
                memories.push(memory)
                showMemories(memories)
            },
            train: ()=>{
                let mems = memories.map((m)=>m.data)
                aa.Train(mems)
            },
            stop:  ()=>{
                isStopped=1
                simulation.stop()
            },
            start: ()=>{
                isStopped = 1
                simulation
                    .force("link",null)
                    .force("center",null)
                    .force("autoassoc",null)
                    .force("autoassoc", (alpha)=>{
                        console.log(`Alpha:${alpha} Min:${simulation.alphaMin()} Target${simulation.alphaTarget()} Decay:${simulation.alphaDecay()} Velocity:${simulation.velocityDecay()}`)
                        let nds = nodes.map((n)=>[n.x,n.y])
                        let ns = nds.flat()
                        let r = aa.Target(ns,alpha*0.1)
                        for(var i=0;i<nodes.length;i++){
                            nodes[i].vx=r.delta[i*2]
                            nodes[i].vy=r.delta[i*2+1]
                            console.log(`${nodes[i].vx}:${nodes[i].vy}`)
                        }
                    })
                simulation.stop()
                simulation.alpha(0.9)
                simulation.alphaTarget(0.1)
                simulation.alphaDecay(0.01)
                simulation.alphaMin(0.2)
                simulation.restart()
            //    let as = []
            //     for(var i=0;i<10;i++){
            //         let newAlpha = (simulation.alphaTarget()-simulation.alpha())*simulation.alphaDecay()
            //         //console.log(`New Alplha: ${newAlpha}`)
            //         simulation
            //         //.restart()
            //         .tick(1)
            //         repaint()
            //         await new Promise((resolve)=>setTimeout(resolve,100))
            //     }
                //console.log(as)
                
            },
            isStopped: ()=>isStopped
        }
    }

    const chart = (simulationMgr) => {
    
        const simulation = forceSimulation(nodes)
            .force("link", forceLink(links).id(d => d.id).distance((link)=>{
                let d = dist(link)
                //console.log(`${link.source.index} ${link.target.index} ${d}`)
                //if(link.source.index===0)return 50
                return d
            }))
            //.force("charge", d3.forceManyBody())
            .force("center", forceCenter(width / 4, height / 2))
    
        simulationMgr.setSimulation(simulation)

        const svg = select("#visual")
            .attr("width", 600)
            .attr("height", 600)
            .attr("style","background-color: grey")
            .attr("viewBox", [0, 0, width, height]);
            
    
        const link = svg.append("g")
            .attr("stroke", "#999")
            .attr("stroke-opacity", 0.6)
        .selectAll("line")
        .data(links)
        .join("line")
            .attr("stroke-width", 0.5);
    
        const node = svg.append("g")
            .attr("stroke", "#fff")
            .attr("stroke-width", 1.5)
        .selectAll("circle")
        .data(nodes)
        .join("circle")
            .attr("r", (d)=>1.5)
            .attr("fill", 'grey')
            .call(dragSetup(simulation,repaint));
    
        function repaint(){

            console.log('.')

            // simulation.force("link",null)
            // simulation.force("link", d3.forceLink(links).id(d => d.id).distance((link)=>{
            //     let d = dist(link)
            //     console.log(`${link.source.index} ${link.target.index} ${d}`)
            //     //if(link.source.index===0)return 50
            //     return d
            // }))

            svg.selectAll("circle")
                .data(nodes)    
                .transition()
                .duration(100)
                .attr("cx",function(d,i){
                    return d.x
                })
                .attr("cy",function(d,i){
                    return d.y
                })

            svg.selectAll("line")
                .data(links)
                .join("line")
                .transition()
                .duration(100)
                .attr("x1",function(d,i){
                    return d.source.x
                })
                .attr("x2",function(d,i){
                    return d.target.x
                })
                .attr("y1",function(d,i){
                    return d.source.y
                })
                .attr("y2",function(d,i){
                    return d.target.y
                })
        }

        node.append("title")
            .text(d => d.id);
    
        simulation.on("tick", () => {
        link
            .attr("x1", d => d.source.x)
            .attr("y1", d => d.source.y)
            .attr("x2", d => d.target.x)
            .attr("y2", d => d.target.y);
    
        node
            .attr("cx", d => d.x)
            .attr("cy", d => d.y);
        });
        simulationMgr.setRepaint(repaint)

    }

    const simulationMgr = SimulationMgr()
    window.simulationMgr = simulationMgr

    function showMemories(memories){
        const parent = select("#memories")
        let buttons = parent.selectAll("button").data(memories)
        buttons.enter()
                    .append("button")
                        .attr("onClick",d=>`simulationMgr.restore(${d.id})`)
                        .text(d=>`Memory ${d.id}`)
        buttons.exit().remove()
    }

    useEffect(()=>{
        
        chart(simulationMgr)
        simulationMgr.forget()

        for(var i=0;i<nodes.length;i++){
            nodes[i].x= 25
            nodes[i].y= 25
        }
        simulationMgr.capture()
        
        for(var i=0;i<nodes.length;i++){
            nodes[i].x= 25
            nodes[i].y= 75
        }
        simulationMgr.capture()
        
        simulationMgr.train()
    })

    return <div>
        <div>
            <button onClick={simulationMgr.forget}>Forget</button>
            <button onClick={simulationMgr.capture}>Capture</button>
            <button onClick={simulationMgr.train}>Train</button>

            <button onClick={simulationMgr.stop}>Stop Simulation</button>
            <button onClick={simulationMgr.start}>Start Simulation</button>
        </div>
        <svg id="visual"></svg>

        <div id="memories"></div>
    </div>
}

render(<App />, document.querySelector('#demo'));

