let debug = ()=>{}

export const AANetwork = (thesize) => {
    const size = thesize
    let weights = []

    let initialize= ()=>{
        for(var i=0;i<size;i++){
            weights[i]= new Array(size)
            for(var j=0;j<size;j++)
                weights[i][j]=0
        }
    }
    initialize()

    return {
        Weights: ()=>weights,
        Train: (memories)=>{
            
            for(var m=0;m<memories.length;m++){
                let memory = memories[m]
                
                for(var i=0;i<size;i++){
                    for(var j=0;j<size;j++){
                        //debug(`${i}:${memory[i]} ${j}:${memory[i]} ${memory[i]*memory[j]}`)
                        weights[i][j]=weights[i][j]+memory[i]*memory[j]
                    }
                }
            }
        }, 
        Compute: (vector)=>{
            debug('Compute')
            const y = new Array(vector.length)
            for(var i=0;i<vector.length;i++) y[i]=0
            
            
            for(i=0;i<vector.length;i++){
                for(var j=0;j<vector.length;j++){
                    y[i] = y[i] + vector[j]*weights[i][j]
                }
            }

            for(i=0;i<vector.length;i++) {
                y[i]= y[i] >0 ? 1 : -1
            }
            
            return y
        }
    }

}
