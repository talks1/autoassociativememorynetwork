import { describe } from 'riteway'

import  { AANetwork } from '../src/aanetwork'
import  { AAForce } from '../src/force'

import {scaleLinear} from 'd3-scale'

export const scale = scaleLinear().domain([25,75]).range([-1,1])

describe('scale', async (assert) => {

    let scale1 = scale(0)
    let scale2 = scale(25)
    let scale3 = scale(50)
    let scale4 = scale(75)
    let scale5 = scale(100)
    assert({
        given: 'a model with delayed processing',
        should: 'be able to complete',
        actual: `${scale1} ${scale2} ${scale3} ${scale4} ${scale5}`,
        expected: '-2 -1 0 1 2'
      })

})

describe('steady state', async (assert) => {

    let training = [
        [25,25,25],
        [75,75,75],
    ]

    let aaforce = AAForce(3,scale,AANetwork,0.1)

    aaforce.Train(training)

    let scenarios = [
        {
            nodes: [25,25,25],
            expectation: '25,25,25'
        },
        {
            nodes: [75,75,75],
            expectation: '75,75,75'
        },
        {
            nodes: [30,30,30],
            expectation: '25,25,25'
        },
        {
            nodes: [70,70,70],
            expectation: '75,75,75'
        },
        {
            nodes: [25,25,75],
            expectation: '25,25,25'
        }
    ]
    
    for(var i in scenarios){
        let scenario = scenarios[i]

        let r = aaforce.Target(scenario.nodes)
        
        assert({
            given: 'a model with delayed processing',
            should: 'be able to complete',
            actual: `${r.target}`,
            expected: scenario.expectation
          })
    }


})