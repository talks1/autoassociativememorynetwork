# AutoAssociateMemoryDemo Standalone Html

## About this component
This project is intended to build a standalone html page. 

It has unit test for the component based on [this](https://medium.com/javascript-scene/unit-testing-react-components-aeda9a44aae2)

## How to use

```
npm install
```

### To Develop

Develop the component by running the server
```
npm run start
```

### To Build

This builds the standalone html file into demo/dist/index.html
```
npm run build
```

### To Test

This test the components by rendering them can checking generated dom.
```
npm run test
```